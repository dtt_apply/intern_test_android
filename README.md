![dtt_logo][logo]

**DTT Android Internship Test**

This is the repository for the DTT Android Internship Test, welcome! In this repository you will find three main directories, as well as a briefing document. 

- **Android Test Brief**: here you will find the answers to everything you need to know regarding the test, how, what, where, when and why. If you're just starting the test, please read this document
- **Data**: contains the needed data assets to complete the test, including a JSON file and set of images 
- **Resources**: contains the variious assets needed to complete the test. Fonts/images/icons appear here
- **Designs**: contains a workflow of how the app should look and function, including a directory of each specific screen in detail

*Should you have an questions and queries that the briefing document hasn't satisfied, please feel free to [email us](mailto:apply@d-tt.nl) with your question*

[logo]: https://bitbucket.org/dtt_apply/intern_test_android/raw/8932f5a1f7df62ab9ee512ae8bdd56b94f2a6ffe/resources/Images/dtt_banner/ldpi/dtt_banner.png